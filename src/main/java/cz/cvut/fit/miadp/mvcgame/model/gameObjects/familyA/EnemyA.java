package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.Random;

public class EnemyA extends AbsEnemy {
    public EnemyA(Position initialPosition) {
        this.position = initialPosition;
    }

    @Override
    public void move( ){
        super.move( );
        Random rand = new Random( );
        int x = rand.nextInt(3) -1;
        int y = rand.nextInt(3) -1;
        this.move( new Vector( x, y ) );
    }
}
