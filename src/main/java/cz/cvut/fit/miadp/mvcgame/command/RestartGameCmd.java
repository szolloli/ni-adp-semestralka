package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public class RestartGameCmd extends AbstractGameCommand{

    public RestartGameCmd(IGameModel subject ) {
        this.subject = subject;
    }
    @Override
    protected void execute() {
        subject.restartGame();
    }
}