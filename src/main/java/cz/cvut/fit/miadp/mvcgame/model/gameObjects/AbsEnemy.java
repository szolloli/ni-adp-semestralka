package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.Random;

public abstract class AbsEnemy extends GameObject{



    @Override
    public void acceptVisitor( IVisitor visitor ){
        visitor.visitEnemy( this );
    }

    public void move() {
        this.move(new Vector(-1, 0));
    }

}
