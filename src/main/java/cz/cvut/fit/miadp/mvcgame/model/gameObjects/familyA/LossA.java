package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameResult;

public class LossA extends AbsGameResult {

    @Override
    public String getResult() {
        return "You lost :(\npress SPACE to restart\npress ESC to quit";
    }
}
