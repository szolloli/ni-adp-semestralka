package cz.cvut.fit.miadp.mvcgame.model;

import java.io.File;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectsFactoryA;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.EnemyA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.LossA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.WinA;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.WaveMovingStrategy;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class GameModel implements IGameModel {

    private AbsCannon cannon;
    private List<AbsMissile> missiles;
    private List<AbsEnemy> enemies;
    private List<AbsCollision> collisions;
    private AbsGameInfo gameInfo;
    private List<IObserver> observers;
    private IGameObjectsFactory goFact;
    private int score;
    private int lives;
    private AbsGameResult gameResult;
    private IMovingStrategy movingStrategy;
    private Random rand = new Random();

    private Queue<AbstractGameCommand> unexecuteCmds = new LinkedBlockingQueue<AbstractGameCommand>( );
    private Stack<AbstractGameCommand> executedCmds = new Stack<AbstractGameCommand>();

    public GameModel( ){
        this.goFact = new GameObjectsFactoryA( this );
        this.observers = new ArrayList<IObserver>();
        restartGame();
    }

    public Position getCannonPosition( ){
        return this.cannon.getPosition( );
    }

    public AbsCannon getCannon(){
        return this.cannon;
    }

    public int getScore() {
        return this.score;
    }

    public int getLives() {
        return this.lives;
    }

    @Override
    public AbsGameResult getGameResult() {
        return this.gameResult;
    }

    @Override
    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public void setLives(int lives) {
        this.lives = lives;
    }


    public void moveCannonDown() {
        this.cannon.moveDown();
        this.notifyObservers( );
    }

    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers( );
    }

    public void aimCannonUp( ){
        this.cannon.aimUp( );
        this.notifyObservers( );
    }

    public void aimCannonDown( ){
        this.cannon.aimDown( );
        this.notifyObservers( );
    }

    public void cannonPowerUp( ){
        this.cannon.powerUp( );
        this.notifyObservers( );
    }

    public void cannonPowerDown( ){
        this.cannon.powerDown( );
        this.notifyObservers( );
    }

    public void restartGame( ) {
        this.cannon = this.goFact.createCannon( );
        this.missiles = new ArrayList<AbsMissile>();
        this.enemies = new ArrayList<AbsEnemy>();
        this.collisions = new ArrayList<AbsCollision>();
        this.gameInfo = this.goFact.createGameInfo( new Position(5,15) );
        this.enemies.add(this.goFact.createEnemy(new Position(MvcGameConfig.MAX_X / 2, MvcGameConfig.MAX_Y / 2 )));
        this.score = 0;
        this.lives = MvcGameConfig.LIVES;
        this.gameResult = null;
        this.movingStrategy = new SimpleMovingStrategy( );
    }

    public void update( ) {
        this.executeCmds( );
        if (this.lives == 0) {
            this.gameResult = new LossA();
            this.notifyObservers();
            return;
        }
        if (this.score == MvcGameConfig.SCORE) {
            this.gameResult = new WinA();
            this.notifyObservers();
            return;
        }
        this.moveMissiles( );
        this.moveEnemies( );
        this.updateCollisions( );
    }

    private void executeCmds( ) {
        while( !this.unexecuteCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.unexecuteCmds.poll( );
            cmd.doExecute( );
            this.executedCmds.push( cmd );
        }
    }

    private void moveMissiles( ){
        List<AbsEnemy> toDelete = new ArrayList<AbsEnemy>();
        List<AbsMissile> usedMissiles = new ArrayList<AbsMissile>();
        for( AbsMissile missile : this.missiles ){
            missile.move( );
            for( AbsEnemy enemy : this.enemies){
                int dX = enemy.getPosition().getX() - missile.getPosition().getX();
                int dY = enemy.getPosition().getY() - missile.getPosition().getY();
                double dist = Math.sqrt(( dX * dX ) + ( dY * dY ) );
                if (dist < 25) {
                    playSound("src/main/resources/sounds/pig.wav");
                    this.collisions.add(goFact.createCollision(enemy.getPosition()));
                    this.enemies.add(goFact.createEnemy(new Position(rand.nextInt(MvcGameConfig.MAX_X/2) + MvcGameConfig.MAX_X/4, rand.nextInt(MvcGameConfig.MAX_Y/2) + MvcGameConfig.MAX_Y/4)));
                    toDelete.add(enemy);
                    usedMissiles.add(missile);
                    this.score += 5;
                    break;
                }
            }
        }
        this.destroyMissiles( usedMissiles );
        this.enemies.removeAll(toDelete);
        this.notifyObservers( );
    }

    private void playSound(String path) {
        Media sound = new Media(new File(path).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
    }

    private void destroyMissiles( List<AbsMissile> usedMissiles ){
        List<AbsMissile> toRemove = new ArrayList<AbsMissile>();
        this.missiles.removeAll( usedMissiles );
        for( AbsMissile missile : this.missiles ){
            if ( missile.getPosition( ).getX( ) > MvcGameConfig.MAX_X ){
                toRemove.add( missile );
            }
        }
        this.missiles.removeAll( toRemove );
    }

    private void destroyEnemies(){
        List<AbsEnemy> toRemove = new ArrayList<AbsEnemy>();
        List<AbsEnemy> toAdd = new ArrayList<AbsEnemy>();
        for( AbsEnemy enemy: this.enemies){
            if ( enemy.getPosition( ).getX( ) < 0 ){
                toRemove.add( enemy );
                toAdd.add(goFact.createEnemy(new Position(rand.nextInt(MvcGameConfig.MAX_X/2) + MvcGameConfig.MAX_X/4, rand.nextInt(MvcGameConfig.MAX_Y/2) + MvcGameConfig.MAX_Y/4)));

            }
        }
        this.enemies.removeAll( toRemove );
        this.enemies.addAll( toAdd );
    }


    private void moveEnemies( ){
        for( AbsEnemy enemy : this.enemies){

            enemy.move( );
            int x = enemy.getPosition().getX();
            if (x < 0) {
                this.lives--;
            }
        }
        this.destroyEnemies( );
        this.notifyObservers( );
    }

    private void updateCollisions( ){
        List<AbsCollision> toRemove = new ArrayList<AbsCollision>();
        for( AbsCollision collision : this.collisions ){
            if ( collision.getAge() > 600 ){
                toRemove.add( collision );
            }
        }
        this.collisions.removeAll( toRemove );
    }

    @Override
    public void registerObserver(IObserver obs) {
        if( !this.observers.contains( obs ) ){
            this.observers.add( obs );
        }
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        if( this.observers.contains( obs ) ){
            this.observers.remove( obs );
        }
        
    }

    @Override
    public void notifyObservers() {
        for( IObserver obs: this.observers){
            obs.update( );
        }
        
    }

    public void cannonShoot( ){
        this.missiles.addAll( cannon.shoot( ) );
        this.notifyObservers( );
    }

    public List<AbsMissile> getMissiles( ) {
        return this.missiles;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> go = new ArrayList<GameObject>();
        go.addAll( this.missiles );
        go.add( this.cannon );
        go.addAll( this.enemies );
        go.addAll( this.collisions );
        go.add( this.gameInfo );
        if (this.gameResult != null)
            go.add( this.gameResult );
        return go;
    }

    public IMovingStrategy getMovingStrategy( ){
        return this.movingStrategy;
    }

    public void toggleMovingStrategy( ){
        if ( this.movingStrategy instanceof SimpleMovingStrategy ){
            this.movingStrategy = new RealisticMovingStrategy( );
        }
        else if ( this.movingStrategy instanceof RealisticMovingStrategy ){
            this.movingStrategy = new WaveMovingStrategy( );
        }
        else {
            this.movingStrategy = new SimpleMovingStrategy( );
        }
    }

    public void toggleShootingMode( ){
        this.cannon.toggleShootingMode( );
    }

    private class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
        private double cannonAngle;
        private int cannonPower;
        private IMovingStrategy movingStrategy;
        private IShootingMode shootingMode;
        private List<AbsEnemy> enemies = new ArrayList<>();
        //TODO GameModel state snapshot

        public void stateSnapshot( ){
            this.score = score;

        }
    }

    public Object createMemento( ) {
        Memento m = new Memento( );
        m.score = this.score;
        m.cannonX = this.getCannonPosition( ).getX( );
        m.cannonY = this.getCannonPosition( ).getY( );
        m.cannonAngle = this.cannon.getAngle();
        m.cannonPower = this.cannon.getPower();
        m.movingStrategy = this.movingStrategy;
        m.shootingMode = this.cannon.getShootingMode();

        Iterator<AbsEnemy> iterator = this.enemies.iterator();
        while(iterator.hasNext()){
            m.enemies.add((EnemyA) iterator.next());
        }
        return m;
    }

    public void setMemento( Object memento ) {
        Memento m = (Memento)memento;
        this.score = m.score;
        this.cannon.getPosition( ).setX( m.cannonX );
        this.cannon.getPosition( ).setY( m.cannonY );
        this.cannon.setAngle( m.cannonAngle );
        this.cannon.setPower( m.cannonPower );
        this.cannon.setShootingMode( m.shootingMode );

        this.enemies = m.enemies;
        this.movingStrategy = m.movingStrategy;
    }

    @Override
    public void registerCommand(AbstractGameCommand cmd) {
        this.unexecuteCmds.add( cmd );
    }

    @Override
    public void undoLastCommand( ) {
        if( !this.executedCmds.isEmpty( ) ){
            AbstractGameCommand cmd = this.executedCmds.pop( );
            cmd.unExecute( );
            this.notifyObservers( );
        }
    }

}
