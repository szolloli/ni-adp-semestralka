package cz.cvut.fit.miadp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith( Suite.class )

@Suite.SuiteClasses( {
        TestCase2.class,
        TestCase3.class,
        TestCaseMock2.class
} )

public class TestSuit2 {

}
