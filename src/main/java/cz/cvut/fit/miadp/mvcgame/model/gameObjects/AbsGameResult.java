package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsGameResult extends GameObject{

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitGameResult(this);
    }

    public abstract String getResult();
}
