package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectsFactoryA;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.CannonA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.GameInfoA;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.WaveMovingStrategy;
import junit.framework.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCaseMock2 {

    @Test
    public void gameInfoTest( ){
        IGameModel model = mock( GameModel.class );
        AbsCannon cannon = mock( CannonA.class );

        when(model.getCannon()).thenReturn( cannon );
        when( cannon.getPower()).thenReturn(1);
        when( cannon.getAngle()).thenReturn(Math.PI);
        when( model.getScore() ).thenReturn( 3 );
        when( model.getLives() ).thenReturn( 3 );
        when( model.getMovingStrategy() ).thenReturn( new WaveMovingStrategy( ) );

        AbsGameInfo gameInfo = new GameInfoA(new Position(100, 2), model);

        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 1, Angle: -180°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 3, Moving strategy: wave"  );
        Assert.assertEquals( gameInfo.getPosition().getX(), 100 );
        Assert.assertEquals( gameInfo.getPosition().getY(), 2 );
    }

}
