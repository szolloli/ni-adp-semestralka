package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.*;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.GameInfoA;
import junit.framework.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class TestCase2 {

    @Test
    public void gameInfoTest( ){

        IGameModel model = new GameModel( );

        AbsGameInfo gameInfo = new GameInfoA(new Position(100, 2), model);
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );

        model.registerCommand( new AimCannonDownCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: -10°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );model.undoLastCommand();
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );

        model.registerCommand( new AimCannonUpCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 10°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );model.undoLastCommand();
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );

        model.registerCommand( new CannonPowerUpCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 11, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );model.undoLastCommand();
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );

        model.registerCommand( new CannonPowerDownCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 9, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );model.undoLastCommand();
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );

        model.registerCommand( new ToggleMovingStrategyCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: realistic" );
        model.registerCommand( new ToggleMovingStrategyCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: wave" );
        model.registerCommand( new ToggleMovingStrategyCmd( model ) );
        model.update( );
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: wave" );
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: realistic" );
        model.undoLastCommand();
        Assert.assertEquals( gameInfo.getText(), "Lives: 3, Power: 10, Angle: 0°, Gravity: "+MvcGameConfig.GRAVITY+", Score: 0, Moving strategy: simple" );
    }

}
