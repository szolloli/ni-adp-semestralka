package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectsFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;

public class CannonA extends AbsCannon {

    private IGameObjectsFactory goFact;

    private double angle;
    private int power;
    private List<AbsMissile> shootingBatch;

    public CannonA( Position initialPosition, IGameObjectsFactory goFact ){
        this.position = initialPosition;
        this.goFact = goFact;

        this.power = MvcGameConfig.INIT_POWER;
        this.angle = MvcGameConfig.INIT_ANGLE;

        this.shootingBatch = new ArrayList<AbsMissile>( );
        this.shootingMode = AbsCannon.SINGLE_SHOOTING_MODE;
    }

    public void moveUp( ){
        this.move( new Vector(0, -1 * MvcGameConfig.MOVE_STEP ) );
    }

    public void moveDown( ){
        this.move( new Vector(0, MvcGameConfig.MOVE_STEP ) );

    }

    @Override
    public List<AbsMissile> shoot( ) {
        this.shootingBatch.clear( );
        this.shootingMode.shoot( this );
        return this.shootingBatch;
    }

    @Override
    public void primitiveShoot( ) {
        this.shootingBatch.add( this.goFact.createMissile( this.angle, this.power ) );
    }

    @Override
    public int getPower() {
        return this.power;
    }

    @Override
    public double getAngle() {
        return this.angle;
    }

    @Override
    public IShootingMode getShootingMode() {
        return this.shootingMode;
    }

    @Override
    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public void setShootingMode(IShootingMode shootingMode) {
        this.shootingMode = shootingMode;
    }

    @Override
    public void aimUp() {
        this.angle -= MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void aimDown() {
        this.angle += MvcGameConfig.ANGLE_STEP;
    }

    @Override
    public void powerUp() {
        this.power += MvcGameConfig.POWER_STEP;
    }

    @Override
    public void powerDown() {
        if ( this.power - MvcGameConfig.POWER_STEP > 0 ){
            this.power -= MvcGameConfig.POWER_STEP;
        }  
    }
    
}
