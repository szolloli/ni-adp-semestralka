package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.*;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.GameInfoA;
import junit.framework.Assert;
import org.junit.Test;

public class TestCase3 {

    @Test
    public void gameResultTest( ){

        IGameModel model = new GameModel( );

        Assert.assertEquals(null, model.getGameResult());

        model.setLives(0);
        model.update();
        Assert.assertNotNull(model.getGameResult());
        Assert.assertEquals("You lost :(\npress SPACE to restart\npress ESC to quit", model.getGameResult().getResult());

        model.restartGame();
        model.setScore(MvcGameConfig.SCORE);
        model.update();
        Assert.assertNotNull(model.getGameResult());
        Assert.assertEquals("You won!\npress SPACE to restart\npress ESC to quit", model.getGameResult().getResult());

   }

}
