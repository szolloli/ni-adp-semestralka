package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import javafx.geometry.Pos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import java.io.File;
import java.io.IOException;

public class GameRenderer implements IVisitor {

    private IGameGraphics gr;

    public void setGraphicContext( IGameGraphics gr ) {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage( "images/cannon.png", cannon.getPosition( ));
        for ( int i = 0; i < 13; i++ ) {
            int x = cannon.getPosition().getX() + 20 + (int)(Math.cos(cannon.getAngle())*i*100) ;
            int y = cannon.getPosition().getY() + 20 + (int)(Math.sin(cannon.getAngle())*i*100) ;
            Position start = new Position(cannon.getPosition().getX() + 20, cannon.getPosition().getY() + 20);
            Position end = new Position((int) (Math.cos(cannon.getAngle())*1300) + cannon.getPosition().getX() + 20, (int) (Math.sin(cannon.getAngle())*1000) + cannon.getPosition().getY() + 20);

            start = new Position(x, y);
            end = new Position(x + (int)(Math.cos(cannon.getAngle())*(70)) , y + (int)(Math.sin(cannon.getAngle())*(70)) );
            this.gr.drawLine(start, end);
        }

    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage( "images/missile.png", missile.getPosition( ) );
        
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        this.gr.drawImage("/images/enemy1.png", enemy.getPosition( ) );
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gr.drawImage("/images/collision.png", collision.getPosition());
    }

    @Override
    public void visitGameInfo(AbsGameInfo gameInfo) {
        this.gr.drawText( gameInfo.getText(), gameInfo.getPosition() );
    }

    @Override
    public void visitGameResult(AbsGameResult gameResult) {
        int centerX = MvcGameConfig.MAX_X / 2;
        int centerY = MvcGameConfig.MAX_Y / 2;
//        this.gr.drawRectangle(new Position(centerX - 50, centerY - 50), new Position(centerX + 50, centerY + 50));
        this.gr.drawText(gameResult.getResult( ), new Position( centerX - 25, centerY ));
    }


}
