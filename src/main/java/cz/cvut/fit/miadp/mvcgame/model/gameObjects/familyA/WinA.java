package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameResult;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public class WinA extends AbsGameResult {

    @Override
    public String getResult() {
        return "You won!\npress SPACE to restart\npress ESC to quit";
    }
}
