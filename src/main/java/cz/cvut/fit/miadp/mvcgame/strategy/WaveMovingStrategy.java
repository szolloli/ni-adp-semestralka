package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public class WaveMovingStrategy implements IMovingStrategy {

    @Override
    public void updatePosition( AbsMissile missile ) {
        int initVelocity = missile.getInitVelocity( );
        double initAngle = missile.getInitAngle( );
        long time = missile.getAge( ) / 100 ;

        int dX = ( int ) ( (Math.cos( time * 30 ) * 10 ) * Math.sin( initAngle ) + ( initVelocity * Math.cos( initAngle ) ) );
        int dY = ( int ) ( (Math.cos( time * 30 ) * 10 ) * Math.cos( initAngle ) + ( initVelocity * Math.sin( initAngle ) ) );

        missile.move( new Vector( dX, dY ) );
    }
    
}
