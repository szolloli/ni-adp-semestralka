package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsGameInfo extends GameObject {
    protected int force;
    protected double angle;
    protected double gravity;
    protected int score;

    public AbsGameInfo(Position position) {
        this.position = position;
    }



    public abstract String getText();
}
