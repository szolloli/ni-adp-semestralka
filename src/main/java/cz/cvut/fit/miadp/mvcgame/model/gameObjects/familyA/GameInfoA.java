package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.WaveMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public class GameInfoA extends AbsGameInfo {
    private IGameModel model;

    public GameInfoA(Position position, IGameModel model) {
        super(position);
        this.model = model;
        this.position = position;
    }

    private long formatAngle(double angle) {
        long fAngle = (-Math.round(Math.toDegrees(angle))) % 360;
        return  fAngle > 180 ? fAngle - 360 : fAngle;
    }

    private String formatMovingStrategy(IMovingStrategy strategy) {
        if (strategy.getClass().isNestmateOf(RealisticMovingStrategy.class)) return "realistic";
        if (strategy.getClass().isNestmateOf(SimpleMovingStrategy.class)) return "simple";
        if (strategy.getClass().isNestmateOf(WaveMovingStrategy.class)) return "wave";
        return "none";
    }

    @Override
    public String getText() {
        return ("Lives: " + this.model.getLives() +
                ", Power: " + this.model.getCannon().getPower() +
                ", Angle: " + formatAngle(this.model.getCannon().getAngle()) +
                "°, Gravity: " + MvcGameConfig.GRAVITY +
                ", Score: " + this.model.getScore()) +
                ", Moving strategy: " + formatMovingStrategy(this.model.getMovingStrategy());
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitGameInfo(this);
    }


}
